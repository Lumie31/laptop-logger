import React from "react";
import "./Header.css";

const Header = () => {
  return (
    <header className="Header">
      <h1 className="Header__brand">Laptop Logger</h1>
    </header>
  );
};

export default Header;
